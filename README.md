# Galactic Dreamscape

Galactic Dreamscape Theme is Mobile-friendly Drupal ^9 || ^10 responsive theme.
This theme features a image slider, Social Media Icon, 2 level menu,
Footer Copyright, Arrow up and Contact Information responsive layout,
multiple column layouts and great for any kind of business website.
Galactic Dreamscape Theme is developed with all latest technologies
Drupal 9 or 10.


## Table of contents

- Theme Features
- Requirements
- Installation
- Configuration
- Maintainer


## Theme Features

- Responsive, Mobile-Friendly Theme
- Mobile support (Smartphone, Tablet, Android, iPhone, etc)
- A total of 11 block regions
- Image Slider with title, description and link
- Sticky header
- 2 Level Drop Down main menus with toggle menu at mobile.


# REQUIREMENTS

No base theme requires.


## INSTALLATION

- Install the Galactic Dreamscape theme as you would normally install 
a contributed Drupal theme.
- Visit https://www.drupal.org/docs/extending-drupal/installing-themes
for contributed Drupal theme.


## Configuration

- Navigate to Administration > Appearance and enable the theme.

- Available options in the theme settings:

 - Change footer copyright text.
 - Change Address, Email ID, phone number in footer.
 - Hide/show bottom to top scroll.
 - Hide/show and change social icons in the footer.
 - Change slider image.

## MAINTAINERS

Current maintainers for Drupal 10:

- Harshita Mehna (Harshita mehna) - https://www.drupal.org/u/harshita-mehna
